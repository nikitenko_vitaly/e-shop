package org.eshop.app.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import org.eshop.app.utils.GoodsManager;
import org.eshop.app.R;
import org.eshop.app.model.Goods;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;


public class GoodsShopAdapter extends RecyclerView.Adapter<GoodsShopAdapter.GoodsViewHolder> {

	private ArrayList<Goods> mDataSet;

	public static class GoodsViewHolder extends RecyclerView.ViewHolder implements Observer {
		private final TextView mTextTitle;
		private final TextView mTextPrice;
		private final TextView mTextPriceTotal;
		private final SeekBar mSeekBarCount;
		private Goods mGoods;

		public GoodsViewHolder(View v) {
			super(v);
			GoodsManager.getInstance().addSeekBarObserver(this);
			mTextTitle = (TextView) v.findViewById(R.id.text_title);
			mTextPrice = (TextView) v.findViewById(R.id.text_price);
			mTextPriceTotal = (TextView) v.findViewById(R.id.text_price_total);
			mSeekBarCount = (SeekBar) v.findViewById(R.id.seekbar_count);
			mSeekBarCount.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
				@Override
				public void onProgressChanged(SeekBar seekBar, int count, boolean fromUser) {
					mTextPriceTotal.setText("Total price: " + String.valueOf(count * mGoods.price));
					GoodsManager.getInstance().notifyGoodsCountChanged(mGoods.id, count);
				}

				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {

				}

				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {

				}
			});
		}

		public void setGoods(Goods goods) {
			mGoods = goods;
			mTextTitle.setText(mGoods.title);
			mTextPrice.setText(String.valueOf(mGoods.price));
		}

		@Override
		public void update(Observable observable, Object data) {
			mSeekBarCount.setProgress(0);
		}
	}

	public GoodsShopAdapter() {
		mDataSet = new ArrayList<>();
	}

	public void setmDataSet(ArrayList<Goods> dataSet) {
		mDataSet = dataSet;
		notifyDataSetChanged();
	}

	@Override
	public GoodsViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
		View v = LayoutInflater.from(viewGroup.getContext())
				.inflate(R.layout.goods_shop_item, viewGroup, false);
		return new GoodsViewHolder(v);
	}

	@Override
	public void onBindViewHolder(GoodsViewHolder viewHolder, final int position) {
		viewHolder.setGoods(mDataSet.get(position));
	}

	@Override
	public int getItemCount() {
		return mDataSet.size();
	}
}
