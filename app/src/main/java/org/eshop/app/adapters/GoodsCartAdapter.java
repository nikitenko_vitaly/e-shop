package org.eshop.app.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.eshop.app.utils.GoodsManager;
import org.eshop.app.R;
import org.eshop.app.model.Goods;


public class GoodsCartAdapter extends RecyclerView.Adapter<GoodsCartAdapter.GoodsViewHolder> {

	public static class GoodsViewHolder extends RecyclerView.ViewHolder {
		private final TextView mTextTitle;
		private final TextView mTextPrice;
		private final TextView mTextTotalPrice;
		private final TextView mTextCount;
		private final Button mBtnRemove;
		private Goods mGoods;

		public GoodsViewHolder(View v, final OnRemoveItemListener onRemoveItemListener) {
			super(v);
			mTextTitle = (TextView) v.findViewById(R.id.text_title);
			mTextPrice = (TextView) v.findViewById(R.id.text_price);
			mTextTotalPrice = (TextView) v.findViewById(R.id.text_total_price);
			mTextCount = (TextView) v.findViewById(R.id.text_goods_count);
			mBtnRemove = (Button) v.findViewById(R.id.btn_remove_item);
			mBtnRemove.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					onRemoveItemListener.onItemRemoved(getPosition());
				}
			});
		}

		public void setGoods(Goods goods, int count) {
			mGoods = goods;
			mTextTitle.setText(mGoods.title);
			mTextPrice.setText(String.valueOf(mGoods.price));
			mTextCount.setText(String.valueOf(count));
			mTextTotalPrice.setText(String.valueOf(mGoods.price*count));
		}
	}

	public GoodsCartAdapter() {
	}

	private interface OnRemoveItemListener {
		public void onItemRemoved(int position);
	}

	private OnRemoveItemListener onRemoveItemListener = new OnRemoveItemListener() {

		@Override
		public void onItemRemoved(int position) {
			removeAt(position);
		}
	};

	private void removeAt(int position) {
		GoodsManager.getInstance().getShoppingCartArray().removeAt(position);
		notifyItemRemoved(position);
	}

	public void removeAll() {
		notifyItemRangeRemoved(0, getItemCount());
		GoodsManager.getInstance().getShoppingCartArray().clear();
	}

	@Override
	public GoodsViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
		View v = LayoutInflater.from(viewGroup.getContext())
				.inflate(R.layout.goods_cart_item, viewGroup, false);
		return new GoodsViewHolder(v, onRemoveItemListener);
	}

	@Override
	public void onBindViewHolder(GoodsViewHolder viewHolder, final int position) {
		int goodsKey = GoodsManager.getInstance().getShoppingCartArray().keyAt(position);
		Goods goods = GoodsManager.getInstance().getGoodsMap().get(goodsKey);
		int goodsCount = GoodsManager.getInstance().getShoppingCartArray().valueAt(position);
		viewHolder.setGoods(goods, goodsCount);
	}

	@Override
	public int getItemCount() {
		return GoodsManager.getInstance().getShoppingCartArray().size();
	}
}
