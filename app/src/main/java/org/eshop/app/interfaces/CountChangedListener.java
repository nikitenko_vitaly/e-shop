package org.eshop.app.interfaces;

public interface CountChangedListener {
	public void onCountChanged(int count);
}
