package org.eshop.app.network;

import android.content.Context;
import android.util.Log;

import com.octo.android.robospice.request.SpiceRequest;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.SerializationUtils;
import org.eshop.app.ui.ShopActivity;
import org.eshop.app.utils.Utils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class GoodsRequest extends SpiceRequest<String> {
	private static final String TAG = "GoodsRequestTAG";
	private final String KEY_LENGTH = "length";
	private final String SHOP_FILENAME = "shop.txt";
	private Context mContext;

	public GoodsRequest(Context context) {
		super(String.class);
		mContext = context;
	}

	@Override
	public String loadDataFromNetwork() throws Exception {
		HttpURLConnection urlConnection = (HttpURLConnection) new URL(ShopActivity.URL_GOODS).openConnection();

		List values = urlConnection.getHeaderFields().get("content-Length");

		String result;
		if (values != null && !values.isEmpty()) {
			String actualLengthStr = (String) values.get(0);
			if (actualLengthStr != null) {
				int storedLength = Utils.getValueFromPreferences(KEY_LENGTH, mContext);
				int actualLength = Integer.parseInt(actualLengthStr);
				if (storedLength != actualLength) {
					Utils.storeToPreferences(KEY_LENGTH, actualLength, mContext);
				} else {
					result = readFromFile();
					Log.d(TAG, "reading from file...");
					urlConnection.disconnect();
					return result;
				}
			}
		}
		Log.d(TAG, "load from network...");
		result = IOUtils.toString(urlConnection.getInputStream());
		urlConnection.disconnect();
		writeToFile(result);
		return result;
	}


	private void writeToFile(String data) {
		try {
			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(mContext.openFileOutput(SHOP_FILENAME, Context.MODE_PRIVATE));
			outputStreamWriter.write(data);
			outputStreamWriter.close();
		}
		catch (IOException e) {
			Log.e("Exception", "File write failed: " + e.toString());
		}
	}


	private String readFromFile() {
		String ret = "";
		try {
			InputStream inputStream = mContext.openFileInput(SHOP_FILENAME);

			if ( inputStream != null ) {
				InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
				BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
				String receiveString = "";
				StringBuilder stringBuilder = new StringBuilder();

				while ( (receiveString = bufferedReader.readLine()) != null ) {
					stringBuilder.append(receiveString);
				}
				inputStream.close();
				ret = stringBuilder.toString();
			}
		}
		catch (FileNotFoundException e) {
			Log.e(TAG, "File not found: " + e.toString());
		} catch (IOException e) {
			Log.e(TAG, "Can not read file: " + e.toString());
		}

		return ret;
	}

	public static long LastModified(String url)	{
		HttpURLConnection.setFollowRedirects(false);
		try {
			HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
			return con.getLastModified();
		} catch (IOException e) {
			e.printStackTrace();
			return 0;
		}
	}
}