package org.eshop.app.utils;

import android.os.Environment;
import android.util.Log;
import android.util.SparseIntArray;

import org.apache.commons.lang3.SerializationUtils;
import org.eshop.app.interfaces.CountChangedListener;
import org.eshop.app.model.Goods;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

public class GoodsManager {
	private static GoodsManager instance = new GoodsManager();
	private CountChangedListener countChangedListener;
	private SparseIntArray mSessionArray;
	private SparseIntArray mShoppingCartArray;
	private HashMap<Integer, Goods> mGoodsMap;
	private SeekBarObserver mSeekBarObserver;
	private int totalSessionPrice = 0;

	public static GoodsManager getInstance() {
		return instance;
	}

	private GoodsManager() {
		mSessionArray = new SparseIntArray();
		mShoppingCartArray = new SparseIntArray();
		mGoodsMap = new HashMap<>();
		mSeekBarObserver = new SeekBarObserver();
	}

	public void notifyGoodsCountChanged(int goodsID, int count) {
		int oldPrice = mGoodsMap.get(goodsID).price * mSessionArray.get(goodsID);
		int newPrice = mGoodsMap.get(goodsID).price * count;
		if (count == 0)
			mSessionArray.delete(goodsID);
		else
			mSessionArray.put(goodsID, count);
		totalSessionPrice += newPrice - oldPrice;
		countChangedListener.onCountChanged(totalSessionPrice);
	}


	public int getCartPrice() {
		int totalCardPrice = 0;
		for (int i = 0; i < mShoppingCartArray.size(); i++) {
			totalCardPrice += mShoppingCartArray.valueAt(i)*mGoodsMap.get(mShoppingCartArray.keyAt(i)).price;
		}
		return totalCardPrice;
	}

	public void updateShoppingCart() {
		for (int i = 0; i < mSessionArray.size(); i++) {
			mShoppingCartArray.put(mSessionArray.keyAt(i),
					mSessionArray.valueAt(i) + mShoppingCartArray.get(mSessionArray.keyAt(i), 0));
		}

		mSessionArray.clear();

		totalSessionPrice = 0;
		mSeekBarObserver.seekBarsRest();
	}

	public void setCountChangedListener(CountChangedListener countChangedListener) {
		this.countChangedListener = countChangedListener;
	}

	public void initGoodsMap(ArrayList<Goods> goodsArrayList) {
		for (Goods goods : goodsArrayList) {
			mGoodsMap.put(goods.id, goods);
		}
	}

	public void addSeekBarObserver(Observer seekBarObserver) {
		mSeekBarObserver.addObserver(seekBarObserver);
	}

	public SparseIntArray getShoppingCartArray() {
		return mShoppingCartArray;
	}

	public HashMap<Integer, Goods> getGoodsMap() {
		return mGoodsMap;
	}

	public void saveShoppingCartArray() {
		String path = Environment.getExternalStoragePublicDirectory( Environment.DIRECTORY_MOVIES).getPath();
		HashMap<Integer, Integer> tmpShoppingCartArray = new HashMap<>();
		for (int i =0; i<mShoppingCartArray.size(); i++) {
			tmpShoppingCartArray.put(mShoppingCartArray.keyAt(i), mShoppingCartArray.valueAt(i));
		}
		try {
			FileOutputStream fos = new FileOutputStream(path + "/shoppingCart.ser");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(tmpShoppingCartArray);
			oos.close();
			fos.close();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

	public void restoreShoppingCartArray() {
		try {
			String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES).getPath();
			FileInputStream fis = new FileInputStream(path + "/shoppingCart.ser");
			ObjectInputStream ois = new ObjectInputStream(fis);
			HashMap<Integer, Integer> tmpShoppingCartArray = (HashMap<Integer, Integer>) ois.readObject();
			ois.close();
			fis.close();
			for (Object o : tmpShoppingCartArray.entrySet()) {
				Map.Entry pair = (Map.Entry) o;
				mShoppingCartArray.put((Integer) pair.getKey(), (Integer) pair.getValue());
			}
		} catch (IOException | ClassNotFoundException ioe) {
			ioe.printStackTrace();
		}
	}

	class SeekBarObserver extends Observable {

		public SeekBarObserver(){}

		public void seekBarsRest() {
			setChanged();
			notifyObservers();
		}
	}
}
