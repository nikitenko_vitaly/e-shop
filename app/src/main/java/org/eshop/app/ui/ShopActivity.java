package org.eshop.app.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.eshop.app.utils.GoodsManager;
import org.eshop.app.R;
import org.eshop.app.adapters.GoodsShopAdapter;
import org.eshop.app.interfaces.CountChangedListener;
import org.eshop.app.model.Goods;
import org.eshop.app.network.GoodsRequest;
import org.eshop.app.network.SimpleSpiceService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class ShopActivity extends BaseActivity {
	private static final String TAG = "ShopActivity";
	private SpiceManager mSpiceManager = new SpiceManager(SimpleSpiceService.class);

	private GoodsRequest mGoodsRequest;
	private GoodsShopAdapter mAdapter;
	private TextView mTextTotalPrice;
//	private TextView textTotalPriceCart;

	//		public static final String URL_GOODS = "https://www.cubbyusercontent.com/pl/items_goods.txt/_c49a614b0d844b4b942efe48f5c89299";
	public static final String URL_GOODS = "https://drive.google.com/uc?export=download&confirm=no_antivirus&id=0B1JpfDTAY0ZmcVRWYnZtZjNXXzA";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		getSupportActionBar().setDisplayHomeAsUpEnabled(false);
		mGoodsRequest = new GoodsRequest(getApplicationContext());
		initDataset();
		GoodsManager.getInstance().restoreShoppingCartArray();
	}

	private void initDataset() {

		RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
		RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
		mRecyclerView.setLayoutManager(mLayoutManager);
		mAdapter = new GoodsShopAdapter();
		mRecyclerView.setAdapter(mAdapter);
		mTextTotalPrice = (TextView) findViewById(R.id.text_price_total_session);
	}

	@Override
	protected void onStart() {
		mSpiceManager.start(this);
		super.onStart();
		mSpiceManager.execute(mGoodsRequest, "txt", DurationInMillis.ONE_SECOND, new ShopRequestListener());
	}

	@Override
	protected void onResume() {
		super.onResume();
		updateTotalPriceCartText();
	}

	@Override
	protected void onStop() {
		mSpiceManager.shouldStop();
		GoodsManager.getInstance().saveShoppingCartArray();
		super.onStop();
	}

	public void addToShoppingCart(View view) {
		GoodsManager.getInstance().updateShoppingCart();
		updateTotalPriceCartText();
	}

	private void updateTotalPriceCartText() {
		if (menuItemTotalPrice != null)
			menuItemTotalPrice.setTitle("Total cart price: " + String.valueOf(GoodsManager.getInstance().getCartPrice()));
	}

	private void initGoodsManager(ArrayList<Goods> goodsList) {
		GoodsManager.getInstance().initGoodsMap(goodsList);
		GoodsManager.getInstance().setCountChangedListener(new CountChangedListener() {
			@Override
			public void onCountChanged(int price) {
				mTextTotalPrice.setText("Total session price: " + String.valueOf(price));
			}
		});
	}

	public final class ShopRequestListener implements RequestListener<String> {

		@Override
		public void onRequestFailure(SpiceException spiceException) {
			Log.e(TAG, "Cannot load goods items");
		}

		@Override
		public void onRequestSuccess(final String result) {
			ArrayList<Goods> goodsList = getGoods(result);
			initGoodsManager(goodsList);
			mAdapter.setmDataSet(goodsList);
			updateTotalPriceCartText();
		}

		private ArrayList<Goods> getGoods(final String result) {
			try {
				ObjectMapper mapper = new ObjectMapper();
				JSONObject jsonObj = new JSONObject(result);
				JSONArray jsonObject = jsonObj.getJSONArray("items");
				return mapper.readValue(jsonObject.toString(), new TypeReference<List<Goods>>() {
				});
			} catch (JSONException | IOException e) {
				Log.e(TAG, "Cannot parse goods items");
				return new ArrayList<>();
			}
		}
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem menuItem) {
		switch (menuItem.getItemId()) {
			case R.id.action_share:
				Intent intent = new Intent(ShopActivity.this, CartActivity.class);
				startActivity(intent);
				return true;
		}
		return super.onOptionsItemSelected(menuItem);
	}

	private MenuItem menuItemTotalPrice;

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu, menu);
		menuItemTotalPrice = menu.findItem(R.id.action_share);
//		updateTotalPriceCartText();
		return true;
	}

}
