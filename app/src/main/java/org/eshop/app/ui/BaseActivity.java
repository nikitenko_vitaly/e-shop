package org.eshop.app.ui;


import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import org.eshop.app.R;

public class BaseActivity extends ActionBarActivity {
	protected Toolbar toolbar;

	@Override
	public void setContentView(View view) {
		super.setContentView(view);
		setupToolbar();
	}

	@Override
	public void setContentView(int layoutResID) {
		super.setContentView(layoutResID);
		setupToolbar();
	}

	private void setupToolbar() {
		toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
	}

	public Toolbar getToolbar() {
		return toolbar;
	}

	@Override
	public void setTitle(CharSequence title) {
		getSupportActionBar().setTitle(title);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem menuItem) {
		switch (menuItem.getItemId()) {
			case android.R.id.home:
				finish();
				return true;
		}
		return super.onOptionsItemSelected(menuItem);
	}
}