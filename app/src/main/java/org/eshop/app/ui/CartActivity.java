package org.eshop.app.ui;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import org.eshop.app.R;
import org.eshop.app.adapters.GoodsCartAdapter;
import org.eshop.app.utils.GoodsManager;
import org.eshop.app.utils.Utils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;


public class CartActivity extends BaseActivity {
	private final int REQUEST_CODE = 2222;
	private final String TEMP_FILENAME = "goods.txt";
	private GoodsCartAdapter mGoodsCartAdapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cart);
		initDataset();
	}

	private void initDataset() {
		RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.shopping_cart_list);
		RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
		mRecyclerView.setLayoutManager(mLayoutManager);
		mGoodsCartAdapter = new GoodsCartAdapter();
		mRecyclerView.setAdapter(mGoodsCartAdapter);

		Button btnSendCustom = (Button) findViewById(R.id.btn_send_custom);
		btnSendCustom.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (GoodsManager.getInstance().getShoppingCartArray().size() != 0) {
					sendEmail(Utils.getPrimaryEmail(CartActivity.this));
					mGoodsCartAdapter.removeAll();
				}
			}
		});
	}

	private String getJsonStringFromCart() {
		int goodsCarSize = GoodsManager.getInstance().getShoppingCartArray().size();
		JSONArray goodsArray = new JSONArray();

		for (int i = 0; i < goodsCarSize; i++) {
			int goodsID = GoodsManager.getInstance().getShoppingCartArray().keyAt(i);
			String goodsTitle = GoodsManager.getInstance().getGoodsMap().get(goodsID).title;
			int goodsCount = GoodsManager.getInstance().getShoppingCartArray().valueAt(i);

			try {
				JSONObject goodsObject = new JSONObject();
				goodsObject.put("title", goodsTitle);
				goodsObject.put("count", goodsCount);
				goodsArray.put(goodsObject);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return goodsArray.toString();
	}

	private void sendEmail(String email) {
		Intent intent = new Intent(android.content.Intent.ACTION_SEND);
		intent.setType("application/octet-stream");
		intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "E-Shop test");
		String to[] = {email};
		intent.putExtra(Intent.EXTRA_EMAIL, to);
		String ownNumber = Utils.getOwnNumber(CartActivity.this);
		if (ownNumber != null)
			intent.putExtra(Intent.EXTRA_TEXT, "My number: " + ownNumber);

		String goodsJsonString = getJsonStringFromCart();

		FileOutputStream fos = null;
		try {
			fos = openFileOutput(TEMP_FILENAME, Context.MODE_WORLD_READABLE);
			fos.write(goodsJsonString.getBytes(), 0, goodsJsonString.getBytes().length);
			fos.flush();
			fos.close();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} finally {
			if (fos != null) try {
				fos.close();
			} catch (IOException ie) {
				ie.printStackTrace();
			}
		}
		File tempDataFile = new File(getFilesDir(), TEMP_FILENAME);

		intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(tempDataFile));
		startActivityForResult(Intent.createChooser(intent, "Send mail..."), REQUEST_CODE);
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == REQUEST_CODE) {
			File tempDataFile = new File(getFilesDir(), TEMP_FILENAME);
			tempDataFile.delete();
		}
	}

}
